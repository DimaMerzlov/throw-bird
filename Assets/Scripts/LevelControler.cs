﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControler : MonoBehaviour
{
    private static int nextLevelIndex=1;
    private Monster[] _enemies;

    private void OnEnable()
    {
        _enemies = FindObjectsOfType<Monster>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach(Monster monster in _enemies)
        {
            if (monster != null)
            {
                return;
            }
            Debug.Log("You killed enemies");
            nextLevelIndex++;
            if (nextLevelIndex >= 5)
            {
                nextLevelIndex = 1;
                SceneManager.LoadScene("Loading");
            }
            else
            {
                string nextLevelName = "Level" + nextLevelIndex;
                SceneManager.LoadScene(nextLevelName);
            }
            
        }
    }
}
