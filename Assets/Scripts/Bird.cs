﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bird : MonoBehaviour
{
    private Vector3 _intialPosition;
    private bool _birdWasLaunched;
    private float _timeSittingAround;


    [SerializeField] private float _launchPower=600f;

    private void Awake()
    {
        _intialPosition = transform.position;
    }
    private void Update()
    {
        GetComponent<LineRenderer>().SetPosition(0, _intialPosition);
        GetComponent<LineRenderer>().SetPosition(1, transform.position);

        if(_birdWasLaunched && GetComponent<Rigidbody2D>().velocity.magnitude<=0.1f)
        {
            _timeSittingAround += Time.deltaTime;
        }

        if (transform.position.y > 10||
            transform.position.y < -20||
            transform.position.x > 18 ||
            transform.position.x < -22||
            _timeSittingAround>3)
        {
            SupportMethod();
        }
        
    }
    private void SupportMethod()//my person method
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
    }

    private void OnMouseDown()
    {
        if (_birdWasLaunched) return;
        GetComponent<SpriteRenderer>().color = Color.red;
        Debug.Log("llllllllllll");
        GetComponent<LineRenderer>().enabled = true;
    }

  
    private void OnMouseUp()
    {
        if (_birdWasLaunched) return;
        GetComponent<SpriteRenderer>().color = Color.white;
        Vector2 directionToInitialPosition = _intialPosition-transform.position;
        GetComponent<Rigidbody2D>().AddForce(directionToInitialPosition*_launchPower);
        GetComponent<Rigidbody2D>().gravityScale = 1;
        _birdWasLaunched = true;
        GetComponent<LineRenderer>().enabled = false;
    }
    private void OnMouseDrag()
    {

        if(_birdWasLaunched) 
            { 
                Debug.Log("isDragMouse = false");
        }
        else
        {
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (newPosition.x >= 0) newPosition.x = 0;
            if (newPosition.y <= -7) newPosition.y = -7;
            transform.position = new Vector3(newPosition.x, newPosition.y);

        }
        Debug.Log("isDragMouse = true");
    }
}
